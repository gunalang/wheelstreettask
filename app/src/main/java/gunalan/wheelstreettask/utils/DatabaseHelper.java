package gunalan.wheelstreettask.utils;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	Context con;

	public DatabaseHelper(Context context, String name, CursorFactory factory,
						  int version) {
		super(context, name, factory, version);
		this.con = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL("create table if not exists session(id integer primary key autoincrement," +
					"userdata list,isread text)");
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int ov, int nv) {
		onCreate(db);

	}

}
