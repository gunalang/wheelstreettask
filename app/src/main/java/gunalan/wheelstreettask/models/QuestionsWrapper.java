package gunalan.wheelstreettask.models;

import java.util.ArrayList;

/**
 * Created by Gunalan GP on 3/8/2018.
 */

public class QuestionsWrapper {

    private String status;
    private ArrayList<QuestionsDataWrapper> data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<QuestionsDataWrapper> getData() {
        return data;
    }

    public void setData(ArrayList<QuestionsDataWrapper> data) {
        this.data = data;
    }
}
