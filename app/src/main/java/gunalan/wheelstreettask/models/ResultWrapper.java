package gunalan.wheelstreettask.models;

/**
 * Created by Gunalan GP on 3/9/2018.
 */

public class ResultWrapper {

    private int status;
    private String data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
