package gunalan.wheelstreettask.models;

/**
 * Created by Gunalan GP on 3/9/2018.
 */

public class AnswerWrapper {

    private int id;
    private String answer;
    private String question;


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
