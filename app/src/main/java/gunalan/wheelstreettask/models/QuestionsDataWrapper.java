package gunalan.wheelstreettask.models;

/**
 * Created by Gunalan GP on 3/8/2018.
 */

public class QuestionsDataWrapper {

    private int id;
    private String question;
    private String dataType;
    private String answer;


    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
