package gunalan.wheelstreettask.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.activity.MainActivity;

import static android.content.Context.MODE_PRIVATE;

public class FbUserDetailsFragment extends Fragment {

    EditText name, email, age, mobile;
    RadioGroup gender;
    ImageView pic;
    String sName, sAge, sEmail, sPic, sGender, sMobile;
    RadioButton male, female;
    Button update, skip;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fb_details, container, false);


        name = (EditText) view.findViewById(R.id.et_name);
        mobile = (EditText) view.findViewById(R.id.et_mobile);
        email = (EditText) view.findViewById(R.id.et_mail);
        age = (EditText) view.findViewById(R.id.et_age);
        pic = (ImageView) view.findViewById(R.id.iv_pic);
        gender = (RadioGroup) view.findViewById(R.id.rb_gender);
        male = (RadioButton) view.findViewById(R.id.rb_male);
        female = (RadioButton) view.findViewById(R.id.rb_female);
        update = (Button) view.findViewById(R.id.btn_update);
        skip = (Button) view.findViewById(R.id.btn_skip);

        SharedPreferences prefs = getActivity().getSharedPreferences("UserDetails", MODE_PRIVATE);
        sName = prefs.getString("name", "");
        sMobile = prefs.getString("mobile", "");
        sEmail = prefs.getString("email", "");
        sGender = prefs.getString("gender", "");
        sPic = prefs.getString("pic", "");
        sAge = prefs.getString("age", "");

        if (!sPic.trim().isEmpty())
            Picasso.with(getActivity()).load("https://graph.facebook.com/" + sPic + "/picture?type=large").into(pic);
        else
            pic.setBackgroundResource(R.drawable.smile_icon);

        name.setText(sName);
        ;
        email.setText(sEmail);
        ;
        age.setText(sAge);

        mobile.setText(sMobile);

        if (sGender.equalsIgnoreCase("male")) {
            male.setChecked(true);
        } else if (sGender.equalsIgnoreCase("female")) {
            female.setChecked(true);
        }


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().trim().isEmpty()) {
                    name.setError("Enter Name");
                } else if (mobile.getText().toString().trim().isEmpty()) {
                    mobile.setError("Enter Mobile");
                } else if (mobile.getText().toString().trim().length() != 10) {
                    mobile.setError("Enter 10 digit Mobile");
                } else if (age.getText().toString().trim().isEmpty()) {
                    age.setError("Enter Age");
                } else if (email.getText().toString().trim().isEmpty()) {
                    email.setError("Enter Email");
                } else if (!isValidEmail(email.getText().toString())) {
                    email.setError("Enter Valid Email");
                } else if (!male.isChecked() && !female.isChecked()) {
                    Toast.makeText(getActivity(), "select Gender", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("UserDetails", MODE_PRIVATE).edit();
                    editor.putString("name", name.getText().toString());
                    editor.putString("pic", sPic);
                    editor.putString("mobile", mobile.getText().toString());
                    editor.putString("age", age.getText().toString());
                    editor.putString("email", email.getText().toString());
                    if (male.isChecked()) {
                        editor.putString("gender", "male");
                    } else if (female.isChecked()) {
                        editor.putString("gender", "female");
                    }
                    editor.apply();

                    QuestionsFragment fragment2 = new QuestionsFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
                    fragmentTransaction.replace(R.id.frame, fragment2);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }


            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new QuestionsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
                fragmentTransaction.replace(R.id.frame, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
