package gunalan.wheelstreettask.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.adapter.QuestionAdapter;
import gunalan.wheelstreettask.models.QuestionsDataWrapper;
import gunalan.wheelstreettask.models.QuestionsWrapper;
import gunalan.wheelstreettask.services.ApiInterface;
import gunalan.wheelstreettask.services.QuestionResult;
import gunalan.wheelstreettask.services.UtilService;
import gunalan.wheelstreettask.utils.AppConstants;
import gunalan.wheelstreettask.utils.MainApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class QuestionsFragment extends Fragment implements QuestionResult {

    RecyclerView recyclerView;
    EditText edittext;
    ProgressDialog progressBar;
    ApiInterface apiService;
    QuestionAdapter questionAdapter;
    ArrayList<QuestionsDataWrapper> questions;
    ArrayList<QuestionsDataWrapper> dummyList = new ArrayList<QuestionsDataWrapper>();
    ArrayList<QuestionsDataWrapper> localData = new ArrayList<QuestionsDataWrapper>();
    int currentPos = 0;
    String shared;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_questions, container, false);

        apiService = MainApplication.getClient().create(ApiInterface.class);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_questions);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ImageView save = (ImageView) view.findViewById(R.id.btn_update);
        edittext = (EditText) view.findViewById(R.id.et_text);

        SharedPreferences prefs1 = getActivity().getSharedPreferences(AppConstants.sharedprefData, Context.MODE_PRIVATE);
        shared = prefs1.getString("result", null);
        if (shared != null) {
            dummyList.clear();
            Gson gson = new Gson();
            dummyList = gson.fromJson(shared, new TypeToken<List<QuestionsDataWrapper>>() {
            }.getType());
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edittext();
            }
        });
        edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    edittext();
                    return true;
                }
                return false;
            }
        });

        if (new UtilService().isNetworkAvailable(getActivity())) {
           // task();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Alert")
                    .setMessage("No Internet Connection")
                    .setCancelable(true)
                    .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            task();
                        }
                    }).show();
        }


        return view;
    }

    private void edittext() {
        if (!edittext.getText().toString().trim().isEmpty()) {
            if (validation(edittext.getText().toString().trim().replace(" ", ""))) {
                dummyList.get(currentPos).setAnswer(edittext.getText().toString());
                if (dummyList.size() <= questions.size()) {
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(AppConstants.sharedprefData, MODE_PRIVATE).edit();
                    if (dummyList.size() > 0) {
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(
                                dummyList, new TypeToken<ArrayList<QuestionsDataWrapper>>() {
                                }.getType());
                        editor.putString("result", jsonElement.toString());
                        editor.apply();
                    }
                    if (questions.size() - 1 > currentPos) {
                        dummyList.add(questions.get(currentPos + 1));
                    }
                }
                if (dummyList.size() > 0) {
                    questionAdapter.notifyItemInserted(dummyList.size() - 1);
                    questionAdapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(dummyList.size());
                    edittext.getText().clear();
                }
            }
            if (questions.size() > 0 && dummyList.size() == questions.size()) {
                for (int j = 0; j < dummyList.size(); j++) {
                    if (dummyList.get(j).getAnswer() == null) {
                        return;
                    }
                }
                hideKeyboard();
                Fragment fragment2 = new AnswerPreviewFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
                fragmentTransaction.replace(R.id.frame, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        } else if (questions.size() > 0 && dummyList.size() == questions.size()) {
            for (int j = 0; j < dummyList.size(); j++) {
                if (dummyList.get(j).getAnswer() == null) {
                    return;
                }
            }
            AnswerPreviewFragment fragment2 = new AnswerPreviewFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.frame, fragment2);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else {
            edittext.setError("Enter your answer");
        }
    }

    private boolean validation(String text) {
        int i = currentPos;
        if (i < questions.size()) {
            if (questions.get(i).getDataType().equalsIgnoreCase("integer")) {
                if (!text.matches("[0-9]+")) {
                    edittext.setError("Enter number value");
                    return false;
                }
            } else if (questions.get(i).getDataType().equalsIgnoreCase("string")) {
                if (!text.matches("[A-Za-z]+")) {
                    edittext.setError("Enter text value");
                    return false;
                }
            } else if (questions.get(i).getDataType().equalsIgnoreCase("boolean")) {
                if (!text.equalsIgnoreCase("yes") &&
                        !text.equalsIgnoreCase("no")) {
                    edittext.setError("Enter yes/no");
                    return false;
                }
            } else if (questions.get(i).getDataType().equalsIgnoreCase("float")) {
                if (!text.matches("\\d+(?:\\.\\d+)?")) {
                    edittext.setError("Enter number value");
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void task() {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.setCancelable(false);
        progressBar.show();
        try {
            Call<QuestionsWrapper> call = apiService.getQuestions();
            call.enqueue(new Callback<QuestionsWrapper>() {
                @Override
                public void onResponse(Call<QuestionsWrapper> call, Response<QuestionsWrapper> response) {
                    if (progressBar != null && progressBar.isShowing())
                        progressBar.dismiss();
                    questions = response.body().getData();
                    if (questions != null && questions.size() > 0) {
                        if (dummyList.size() > 0) {
                            if (dummyList.size() < questions.size() && dummyList.get(dummyList.size() - 1).getAnswer() != null)
                                dummyList.add(questions.get(dummyList.size()));
                            questionAdapter = new QuestionAdapter(dummyList, R.layout.questions_ans_items, getActivity(), QuestionsFragment.this);
                            recyclerView.setAdapter(questionAdapter);
                            for (int i = 0; i < dummyList.size(); i++) {
                                if (dummyList.get(i).getAnswer() == null) {
                                    return;
                                }
                            }
                        } else {
                            dummyList.clear();
                            dummyList.add(questions.get(currentPos));
                            questionAdapter = new QuestionAdapter(dummyList, R.layout.questions_ans_items, getActivity(), QuestionsFragment.this);
                            recyclerView.setAdapter(questionAdapter);
                        }
                    } else {
                        noDataAlert();
                    }
                }

                @Override
                public void onFailure(Call<QuestionsWrapper> call, Throwable t) {
                    // Log error here since request failed
                    if (progressBar != null && progressBar.isShowing())
                        progressBar.dismiss();
                    Log.e("error", t.toString());
                    noDataAlert();
                }
            });
        } catch (Exception e) {
            if (progressBar != null && progressBar.isShowing())
                progressBar.dismiss();
            noDataAlert();
            e.printStackTrace();
        }
    }

    @Override
    public void answer(int pos) {
        currentPos = pos;
    }

    private void noDataAlert() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert")
                .setMessage("something went wrong")
                .setCancelable(true)
                .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        task();
                    }
                }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }
}
