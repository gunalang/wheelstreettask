package gunalan.wheelstreettask.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.services.UtilService;

import static android.content.Context.MODE_PRIVATE;

public class FbLoginFragment extends Fragment {

    private static final String EMAIL = "email";
    CallbackManager callbackManager;
    String firstName, lastName, gender, email, pic;
    LinearLayout fb;
    Button localData, skip;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fb_login, container, false);

        FacebookSdk.sdkInitialize(getActivity());
        AppEventsLogger.activateApp(getActivity());
        printHashKey();
        fb = (LinearLayout) view.findViewById(R.id.btn_fb);
        skip = (Button) view.findViewById(R.id.btn_skip);
        localData = (Button) view.findViewById(R.id.btn_local);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new FbUserDetailsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
                fragmentTransaction.replace(R.id.frame, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        localData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new LocalDataFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
                fragmentTransaction.replace(R.id.frame, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        callbackManager = CallbackManager.Factory.create();

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new UtilService().isNetworkAvailable(getActivity())) {
                    login();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setMessage("No Internet Connection")
                            .setCancelable(false)
                            .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    login();
                                }
                            }).show();
                }
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        setFacebookData(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        return view;
    }

    private void login() {
        LoginManager.getInstance().logInWithReadPermissions(
                FbLoginFragment.this,
                Arrays.asList("user_friends", "email", "user_birthday", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        setFacebookData(loginResult);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setFacebookData(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        String id = null;
                        try {
                            Log.i("Response", response.toString());

                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null) {
                                id = profile.getId();
                                String link = profile.getLinkUri().toString();
                                Log.i("Link", link);
                                if (Profile.getCurrentProfile() != null) {
                                    Log.i("Login", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                                }

                                pic = Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString();
                                firstName = response.getJSONObject().getString("first_name");
                                lastName = response.getJSONObject().getString("last_name");
                                gender = response.getJSONObject().getString("gender");
                            } else {
                                Toast.makeText(getActivity(), "Profile null, try again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {

                            if (id != null) {
                                SharedPreferences.Editor editor = getActivity().getSharedPreferences("UserDetails", MODE_PRIVATE).edit();
                                editor.putString("name", firstName + " " + lastName);
                                editor.putString("pic", id);
                                editor.putString("age", "");
                                editor.putString("mobile", "");
                                editor.putString("email", email);
                                editor.putString("gender", gender);
                                editor.apply();

                                FbUserDetailsFragment fragment2 = new FbUserDetailsFragment();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.pop_enter, R.anim.pop_exit);
                                fragmentTransaction.replace(R.id.frame, fragment2);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            } else {
                                Toast.makeText(getActivity(), "Profile null, try again", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void printHashKey() {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("key", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("error", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("error", "printHashKey()", e);
        }
    }

}
