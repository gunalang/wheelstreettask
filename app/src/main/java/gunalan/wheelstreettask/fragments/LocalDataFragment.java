package gunalan.wheelstreettask.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.adapter.LocalDataAdapter;
import gunalan.wheelstreettask.models.AnswerWrapper;
import gunalan.wheelstreettask.models.QuestionsDataWrapper;
import gunalan.wheelstreettask.models.QuestionsWrapper;
import gunalan.wheelstreettask.models.ResultWrapper;
import gunalan.wheelstreettask.services.ApiInterface;
import gunalan.wheelstreettask.services.QuestionResult;
import gunalan.wheelstreettask.services.UtilService;
import gunalan.wheelstreettask.utils.DatabaseHelper;
import gunalan.wheelstreettask.utils.MainApplication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class LocalDataFragment extends Fragment implements QuestionResult {

    RecyclerView recyclerView;
    ProgressDialog progressBar;
    ApiInterface apiService;
    Button save;
    LocalDataAdapter answerAdapter;
    ArrayList<QuestionsDataWrapper> questionsAnswer;
    String sName, sAge, sEmail, sPic, sGender, sMobile;
    DatabaseHelper db;
    SQLiteDatabase sdb;
    long delayTime = 3500;
    Handler handler = new Handler();
    JsonObject jsonObject;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_answers, container, false);

        apiService = MainApplication.getClient().create(ApiInterface.class);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_questions);
        save = (Button) view.findViewById(R.id.btn_Save);
        save.setVisibility(View.GONE);
        try {
            db = new DatabaseHelper(getActivity(), "db", null, 1);
            sdb = db.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Cursor cursor = sdb.rawQuery("select * from session", null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            Gson gson = new Gson();
            jsonObject = gson.fromJson(
                    cursor.getString(cursor.getColumnIndex("userdata")),
                    new TypeToken<JsonObject>() {
                    }.getType());
            String isread = gson.fromJson(
                    cursor.getString(cursor.getColumnIndex("isread")),
                    new TypeToken<String>() {
                    }.getType());
            delayTime = 5000;
        }

        SharedPreferences prefs = getActivity().getSharedPreferences("UserDetails", MODE_PRIVATE);
        sName = prefs.getString("name", "");
        sMobile = prefs.getString("mobile", "");
        sEmail = prefs.getString("email", "");
        sGender = prefs.getString("gender", "");
        sPic = prefs.getString("pic", "");
        sAge = prefs.getString("age", "");
        if (jsonObject != null) {
            JsonObject json = new JsonObject();
            Gson gson = new Gson();
            json = gson.fromJson(jsonObject, new TypeToken<JsonObject>() {
            }.getType());

            questionsAnswer = gson.fromJson(json.get("questions"), new TypeToken<ArrayList<QuestionsDataWrapper>>() {
            }.getType());
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (questionsAnswer != null && questionsAnswer.size() > 0) {
            answerAdapter = new LocalDataAdapter(questionsAnswer, R.layout.questions_items, getActivity(), LocalDataFragment.this);
            recyclerView.setAdapter(answerAdapter);
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("No Data")
                    .setMessage("No Data saved yet!!!")
                    .setCancelable(true)
                    .setPositiveButton("close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }


        return view;
    }

    private void task() {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.setCancelable(false);
        progressBar.show();
        ArrayList<AnswerWrapper> answerWrapperArrayList = new ArrayList<AnswerWrapper>();
        for (int i = 0; i < questionsAnswer.size(); i++) {
            AnswerWrapper answerWrapper = new AnswerWrapper();
            answerWrapper.setId(questionsAnswer.get(i).getId());
            answerWrapper.setAnswer(questionsAnswer.get(i).getAnswer());
            answerWrapperArrayList.add(answerWrapper);
        }
        JsonObject jsonObject = new JsonObject();
        Gson gson = new Gson();
        JsonArray myCustomArray = gson.toJsonTree(answerWrapperArrayList).getAsJsonArray();
        jsonObject.add("questions", myCustomArray);
        jsonObject.addProperty("id", sPic);
        jsonObject.addProperty("name", sName);
        jsonObject.addProperty("fbUserName", sName);
        jsonObject.addProperty("mobile", sMobile);
        jsonObject.addProperty("gender", sGender);
        jsonObject.addProperty("age", sAge);
        jsonObject.addProperty("email", sEmail);

        try {
            Call<ResultWrapper> call = apiService.postQuestionsAnswer(jsonObject);
            call.enqueue(new Callback<ResultWrapper>() {
                @Override
                public void onResponse(Call<ResultWrapper> call, Response<ResultWrapper> response) {
                    if (progressBar != null && progressBar.isShowing())
                        progressBar.dismiss();
                    Toast.makeText(getActivity(), "Data saved to Database", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResultWrapper> call, Throwable t) {
                    // Log error here since request failed
                    if (progressBar != null && progressBar.isShowing())
                        progressBar.dismiss();
                    Log.e("error", t.toString());
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setMessage("something went wrong")
                            .setCancelable(true)
                            .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    task();
                                }
                            }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
            });
        } catch (Exception e) {
            if (progressBar != null && progressBar.isShowing())
                progressBar.dismiss();
            new AlertDialog.Builder(getActivity())
                    .setTitle("Alert")
                    .setMessage("something went wrong")
                    .setCancelable(true)
                    .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            task();
                        }
                    }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
            e.printStackTrace();
        }
    }


    @Override
    public void answer(int pos) {

    }
}
