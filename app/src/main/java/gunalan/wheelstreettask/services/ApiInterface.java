package gunalan.wheelstreettask.services;

import com.google.gson.JsonObject;

import gunalan.wheelstreettask.models.QuestionsWrapper;
import gunalan.wheelstreettask.models.ResultWrapper;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Gunalan GP on 3/8/2018.
 */

public interface ApiInterface {

    @GET("v1/test/questions")
    @Headers("Accept: application/json")
    Call<QuestionsWrapper> getQuestions();

    @POST("v1/test/answers")
    @Headers("Accept: application/json")
    Call<ResultWrapper> postQuestionsAnswer(@Body JsonObject jsonObject);

}
