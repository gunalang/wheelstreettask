package gunalan.wheelstreettask.services;

import java.util.ArrayList;
import java.util.List;

import gunalan.wheelstreettask.models.QuestionsDataWrapper;

/**
 * Created by Gunalan GP on 3/9/2018.
 */

public interface QuestionResult {

    void answer(int pos);

}
