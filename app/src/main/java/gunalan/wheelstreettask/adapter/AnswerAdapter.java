package gunalan.wheelstreettask.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.models.QuestionsDataWrapper;
import gunalan.wheelstreettask.services.QuestionResult;

/**
 * Created by Gunalan GP on 3/8/2018.
 */

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.QuestionHolder> {

    private ArrayList<QuestionsDataWrapper> questions;
    private int rowLayout;
    private Context context;

    public static class QuestionHolder extends RecyclerView.ViewHolder {
        RelativeLayout moviesLayout;
        TextView question;
        TextView answer;


        public QuestionHolder(View v) {
            super(v);
            moviesLayout = (RelativeLayout) v.findViewById(R.id.ll_item);
            question = (TextView) v.findViewById(R.id.tv_question);
            answer = (TextView) v.findViewById(R.id.et_answer);
        }
    }

    public AnswerAdapter(ArrayList<QuestionsDataWrapper> questions, int rowLayout, Context context) {
        this.questions = questions;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public QuestionHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new QuestionHolder(view);
    }


    @Override
    public void onBindViewHolder(QuestionHolder holder, final int position) {
        if (questions.get(position).getQuestion() != null)
            holder.question.setText(questions.get(position).getQuestion());
        else
            Toast.makeText(context, "empty",Toast.LENGTH_SHORT).show();
        if (questions.get(position).getQuestion() != null)
            holder.answer.setText(questions.get(position).getAnswer());
        else
            Toast.makeText(context, "empty",Toast.LENGTH_SHORT).show();

    }

    @Override
    public int getItemCount() {
        return questions.size();
    }


}