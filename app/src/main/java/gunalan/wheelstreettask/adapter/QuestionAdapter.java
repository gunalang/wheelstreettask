package gunalan.wheelstreettask.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.models.QuestionsDataWrapper;
import gunalan.wheelstreettask.services.QuestionResult;

/**
 * Created by Gunalan GP on 3/8/2018.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionHolder> {

    private ArrayList<QuestionsDataWrapper> questions;
    private int rowLayout;
    private Context context;
    QuestionResult questionResult;
    private int lastAnimatedPosition = -1;
    private boolean animationsLocked = false;
    private boolean delayEnterAnimation = true;

    public static class QuestionHolder extends RecyclerView.ViewHolder {
        RelativeLayout answerFrame;
        TextView question;
        TextView answer;
        public MyCustomEditTextListener myCustomEditTextListener;


        public QuestionHolder(View v, MyCustomEditTextListener myCustomEditTextListener) {
            super(v);
            answerFrame = (RelativeLayout) v.findViewById(R.id.rl_answer);
            question = (TextView) v.findViewById(R.id.tv_question);
            answer = (TextView) v.findViewById(R.id.et_answer);
            this.myCustomEditTextListener = myCustomEditTextListener;
            this.answer.addTextChangedListener(myCustomEditTextListener);
        }
    }

    public QuestionAdapter(ArrayList<QuestionsDataWrapper> questions, int rowLayout, Context context, QuestionResult questionResult) {
        this.questions = questions;
        this.rowLayout = rowLayout;
        this.context = context;
        this.questionResult = questionResult;
    }

    @Override
    public QuestionHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new QuestionHolder(view, new MyCustomEditTextListener());
    }


    @Override
    public void onBindViewHolder(QuestionHolder holder, final int position) {
        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
        if (questions.get(position).getQuestion() != null)
            holder.question.setText(questions.get(position).getQuestion());
        else
            Log.e("tag", "empty");
        if (questions.get(position).getAnswer() != null) {
            holder.answer.setText(questions.get(position).getAnswer());
            holder.answerFrame.setVisibility(View.VISIBLE);
        } else {
            holder.answerFrame.setVisibility(View.GONE);
        }
        runEnterAnimation(holder.question, position);
        questionResult.answer(position);
    }

    private void runEnterAnimation(View view, int position) {
        if (animationsLocked) return;

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(100);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animationsLocked = true;
                        }
                    })
                    .start();
        }
    }

    public void setAnimationsLocked(boolean animationsLocked) {
        this.animationsLocked = animationsLocked;
    }


    @Override
    public int getItemCount() {
        return questions.size();
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            QuestionsDataWrapper questionsDataWrapper = new QuestionsDataWrapper();
            questionsDataWrapper.setAnswer(charSequence.toString());
            questionsDataWrapper.setQuestion(questions.get(position).getQuestion());
            questions.set(position, questionsDataWrapper);
            questionResult.answer(position);
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }
}