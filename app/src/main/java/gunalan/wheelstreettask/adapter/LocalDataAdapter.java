package gunalan.wheelstreettask.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.models.QuestionsDataWrapper;
import gunalan.wheelstreettask.services.QuestionResult;

/**
 * Created by Gunalan GP on 3/8/2018.
 */

public class LocalDataAdapter extends RecyclerView.Adapter<LocalDataAdapter.QuestionHolder> {

    private ArrayList<QuestionsDataWrapper> questions;
    private int rowLayout;
    private Context context;
    QuestionResult questionResult;

    public static class QuestionHolder extends RecyclerView.ViewHolder {
        RelativeLayout moviesLayout;
        TextView question;
        TextView answer;
        public MyCustomEditTextListener myCustomEditTextListener;


        public QuestionHolder(View v, MyCustomEditTextListener myCustomEditTextListener) {
            super(v);
            moviesLayout = (RelativeLayout) v.findViewById(R.id.ll_item);
            question = (TextView) v.findViewById(R.id.tv_question);
            answer = (TextView) v.findViewById(R.id.et_answer);
            this.myCustomEditTextListener = myCustomEditTextListener;
            this.answer.addTextChangedListener(myCustomEditTextListener);
        }
    }

    public LocalDataAdapter(ArrayList<QuestionsDataWrapper> questions, int rowLayout, Context context, QuestionResult questionResult) {
        this.questions = questions;
        this.rowLayout = rowLayout;
        this.context = context;
        this.questionResult = questionResult;
    }

    @Override
    public QuestionHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new QuestionHolder(view, new MyCustomEditTextListener());
    }


    @Override
    public void onBindViewHolder(QuestionHolder holder, final int position) {
        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
        if (questions.get(position).getQuestion() != null)
            holder.question.setText(questions.get(position).getQuestion());
        else
            Log.e("tag ", "Empty");
        if (questions.get(position).getAnswer() != null)
            holder.answer.setText(questions.get(position).getAnswer());
        else
            Log.e("tag ", "Empty");
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            QuestionsDataWrapper questionsDataWrapper = new QuestionsDataWrapper();
            questionsDataWrapper.setAnswer(charSequence.toString());
            questionsDataWrapper.setQuestion(questions.get(position).getQuestion());
            questions.set(position, questionsDataWrapper);
            questionResult.answer(position);
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }
}