package gunalan.wheelstreettask.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import gunalan.wheelstreettask.R;
import gunalan.wheelstreettask.fragments.FbLoginFragment;
import gunalan.wheelstreettask.utils.ClearfragmentListener;
import gunalan.wheelstreettask.utils.SwipeBackActivity;
import gunalan.wheelstreettask.utils.SwipeBackLayout;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

        frameLayout = (FrameLayout) findViewById(R.id.frame);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new FbLoginFragment();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();

       // setDragEdge(SwipeBackLayout.DragEdge.LEFT);

    }

    @Override
    public void onBackPressed() {
      //  MainActivity.this.overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        super.onBackPressed();
    }

}
